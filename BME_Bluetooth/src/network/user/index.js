import firebase from '../../firebase/config'

const AddUser = async (name, email, password, profileImg) => {
    let encode = '';
    for (let i = 0; i < email.length; i++) {
        let x = email.slice(i, i + 1);
        encode += x.charCodeAt(0);
    }
    
    try {
        return await firebase
            .database()
            .ref(encode)
            .set({
                name: name,
                email: email,
                save: password,
                profileImg: profileImg,
            }
            )
    } catch (error) {
        return error;
    }
};

export default AddUser;