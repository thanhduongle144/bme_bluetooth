
import React, { useReducer } from 'react';
import { Loader,UserInfo } from '../reducers';

export const Store = React.createContext();
const dispatch = {};

export function StoreProvider(props) {
    const [mapLoaderState, dispatchLoaderAction] = useReducer(Loader, dispatch);
    const loaderValue = { mapLoaderState, dispatchLoaderAction };
    const [mapUserInfoState, dispatchUserInfoAction] = useReducer(UserInfo, dispatch);
    const userInfoValue = { mapUserInfoState, dispatchUserInfoAction };
    const value = {
        ...loaderValue,
        ...userInfoValue,
    };

    return <Store.Provider value={value}>{props.children}</Store.Provider>;
}