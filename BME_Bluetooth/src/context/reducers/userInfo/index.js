import {USER_DATA} from '../../actions/types'

const initialState ={
    email:'',
    name:'',
    decode:'',
    profileImg:'',
};

const userInfo = (state = initialState, action) => {
    switch (action.type) {
        case USER_DATA:{
            return{
                ...state,
                email:action.email,
                name:action.name,
                decode:action.decode,
                profileImg:action.profileImg
            }
        };
        default:
            return state;
    }
};

export default userInfo;