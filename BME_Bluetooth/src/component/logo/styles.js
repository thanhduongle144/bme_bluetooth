
import { StyleSheet } from "react-native";
import { color, appStyle } from "../../utility";
import { smallDeviceHeight } from "../../utility/constants";

const getDimensions = () => {
  if (appStyle.deviceHeight > smallDeviceHeight) {
    return {
      height: 200,
      width: 200,
      borderRadius: 100,
      logoFontSize: 90,
    };
  } else {
    return {
      height: 150,
      width: 150,
      borderRadius: 100,
      logoFontSize: 70,
    };
  }
};

export default StyleSheet.create({
  logo: {
    height: getDimensions().height,
    width: getDimensions().width,
    borderRadius: getDimensions().borderRadius,
    backgroundColor: color.DARK_GRAY,
    alignItems: "center",
    justifyContent: "center",
    flexDirection:'row'
  },
  text: {
    fontSize: getDimensions().logoFontSize,
    fontWeight: "bold",
    color: color.WHITE,
  },
});