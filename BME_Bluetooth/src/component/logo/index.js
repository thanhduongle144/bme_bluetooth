import React from 'react'
import { Text, View } from "react-native";
import styles from './styles'
import Icon from 'react-native-vector-icons/AntDesign'
import { color } from '../../utility'

export default ({ logoStyle, logoTextStyle }) => (
    <View style={[styles.logo, logoStyle]}>
        <Icon style={{ color: color.WHITE }} name='dingding' size={styles.text.fontSize} />
        <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
            <Text style={[styles.text, logoTextStyle]}>T</Text>
            <Text style={{color:color.WHITE}}>Phong Thanh</Text>
            <Text style={{color:color.WHITE,fontSize:styles.text.fontSize/8}}>~Promise~</Text>
        </View>

        <Icon style={{ color: color.WHITE, transform: [{ rotateY: '180deg' }] }} name='dingding' size={styles.text.fontSize} />
    </View>
)