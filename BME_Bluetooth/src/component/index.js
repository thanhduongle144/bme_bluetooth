import Logo from './logo'
import InputField from './input'
import RoundCornerButton from './button/RoundCornerButton'
import Profile from './profile'
import ShowUsers from './showUsers'
import StickyHeader from './stickyHeader'
const decodeEmail = ( email ) => {
    let encode = '';
    for (let i = 0; i < email.length; i++) {
        let x = email.slice(i, i + 1);
        encode += x.charCodeAt(0);
    }
    return encode;
}
export { Logo, InputField, RoundCornerButton, Profile, ShowUsers, StickyHeader, decodeEmail };