import React, { useContext, useEffect, useState, useLayoutEffect } from "react";
import { SafeAreaView, Alert, Text, View, FlatList } from "react-native";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import ImagePicker from "react-native-image-picker";
import { Profile, ShowUsers, StickyHeader, decodeEmail } from "../../component";
import firebase from "../../firebase/config";
import { color } from "../../utility";
import { Store } from "../../context/store";
import { LOADING_STOP, LOADING_START } from "../../context/actions/types";
import { uuid, smallDeviceHeight } from "../../utility/constants";
// import { clearAsyncStorage } from "../../asyncStorage";
import { deviceHeight } from "../../utility/styleHelper/appStyle";
import { AddUser } from "../../network";
import AsyncStorage from "@react-native-community/async-storage";
import { clearAsyncStorage } from "../../asyncStorage";
import { endAsyncEvent } from "react-native/Libraries/Performance/Systrace";
// import { UpdateUser, LogOutUser } from "../../network";

export default ({ navigation }) => {
  const globalState = useContext(Store);
  const { dispatchLoaderAction, mapUserInfoState } = globalState;

  const [userDetail, setUserDetail] = useState({
    id: "",
    name: '',
    profileImg: "",
  });
  const [getScrollPosition, setScrollPosition] = useState(0);
  const [allUsers, setAllUsers] = useState([]);
  const { profileImg, name } = userDetail;
  useLayoutEffect(() => {
    // console.warn('Load user is:'+JSON.stringify(mapUserInfoState));
    navigation.setOptions({
      headerRight: () => (
        <SimpleLineIcons
          name="logout"
          size={26}
          color={color.WHITE}
          style={{ right: 10 }}
          onPress={() =>
            Alert.alert(
              "Logout",
              "Are you sure to log out",
              [
                {
                  text: "Yes",
                  onPress: () => logout(),
                },
                {
                  text: "No",
                },
              ],
              { cancelable: false }
            )
          }
        />
      ),
    });
  }, [navigation]);

  useEffect(() => {
    //   console.warn(UserData.name)
    dispatchLoaderAction({
      type: LOADING_START,
    });
    const highestTimeoutId = setTimeout(() => ';');
    for (let i = 0; i < highestTimeoutId; i++) {
      clearTimeout(i);
    }
    try {
      // console.warn('Data save ='+JSON.stringify(AsyncStorage.getItem('user')))
      let encode = decodeEmail(mapUserInfoState.email);
      let listener2 = firebase.database().ref(encode)
        listener2.once('value')
        .then(snapshot => {
          console.log(snapshot)
          setUserDetail({
            ...userDetail,
            name: mapUserInfoState.name,
            profileImg: mapUserInfoState.profileImg
          });
          firebase.database().ref.off(endcode, listener2);
        })
      // firebase
      //   .database()
      //   .ref("users")
      //   .on("value", (dataSnapshot) => {
      //     let users = [];
      //     let currentUser = {
      //       id: "",
      //       name: "",
      //       profileImg: "",
      //     };
      //     dataSnapshot.forEach((child) => {
      //       if (uuid === child.val().uuid) {
      //         currentUser.id = uuid;
      //         currentUser.name = child.val().name;
      //         currentUser.profileImg = child.val().profileImg;
      //       } else {
      //         users.push({
      //           id: child.val().uuid,
      //           name: child.val().name,
      //           profileImg: child.val().profileImg,
      //         });
      //       }
      //     });
      //     setUserDetail(currentUser);
      //     setAllUsers(users);
      //     dispatchLoaderAction({
      //       type: LOADING_STOP,
      //     });
      //   });
    } catch (error) {
      alert(error);
      dispatchLoaderAction({
        type: LOADING_STOP,
      });
    }
  }, []);

  const selectPhotoTapped = () => {
    AsyncStorage.getItem('user', (data) => {
      console.warn(data)
    })
    const options = {
      title: 'Select Avatar',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled photo picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        // Base 64 image:
        let source = "data:image/jpeg;base64," + response.data;
        dispatchLoaderAction({
          type: LOADING_START,
        });

        let encode = decodeEmail(mapUserInfoState.email);
        let listener = firebase.database().ref(encode)
          listener.once('value')
          .then(snapshot => {
            // console.warn(mapUserInfoState.name+'\n'+mapUserInfoState.email+'\n'+snapshot.val().password+'\n'+source)
            AddUser(name, mapUserInfoState.email, snapshot.val().save, source)
              .then(() => {
                setUserDetail({
                  ...userDetail,
                  profileImg: source,
                });
                dispatchLoaderAction({
                  type: LOADING_STOP,
                });
                firebase.database().ref.off(encode,listener);
              })
          })
      }
    });
  };
  // * LOG OUT
  const logout = async () => {
    // LogOutUser()
    //   .then(() => {
    // clearAsyncStorage()
    //   .then(() => {
    await clearAsyncStorage();
    navigation.replace("Login");
    //   })
    //   .catch((err) => console.log(err));
    //   })
    //   .catch((err) => alert(err));
  };

  // * ON IMAGE TAP
  const imgTap = (profileImg, name) => {
    if (!profileImg) {
      //   navigation.navigate("ShowFullImg", {
      //     name,
      //     imgText: name.charAt(0),
      //   });
      alert('no image')
    } else {
      //   navigation.navigate("ShowFullImg", { name, img: profileImg });
    }
  };

  // * ON NAME TAP
  const nameTap = (profileImg, name, guestUserId) => {
    // if (!profileImg) {
    //   navigation.navigate("Chat", {
    //     name,
    //     imgText: name.charAt(0),
    //     guestUserId,
    //     currentUserId: uuid,
    //   });
    // } else {
    //   navigation.navigate("Chat", {
    //     name,
    //     img: profileImg,
    //     guestUserId,
    //     currentUserId: uuid,
    //   });
    // }
  };
  // * GET OPACITY

  const getOpacity = () => {
    if (deviceHeight < smallDeviceHeight) {
      return deviceHeight / 4;
    } else {
      return deviceHeight / 6;
    }
  };
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: color.BLACK }}>
      {getScrollPosition > getOpacity() && (
        <StickyHeader
          name={name}
          img={profileImg}
          onImgTap={() => imgTap(profileImg, name)}
        />
      )}

      {/* ALL USERS */}
      <FlatList
        alwaysBounceVertical={false}
        data={allUsers}
        keyExtractor={(_, index) => index.toString()}
        onScroll={(event) =>
          setScrollPosition(event.nativeEvent.contentOffset.y)
        }
        ListHeaderComponent={
          <View
            style={{
              opacity:
                getScrollPosition < getOpacity()
                  ? (getOpacity() - getScrollPosition) / 100
                  : 0,
            }}
          >
            <Profile
              img={profileImg}
              onImgTap={() => imgTap(profileImg, mapUserInfoState.name)}
              onEditImgTap={() => selectPhotoTapped()}
              name={mapUserInfoState.name}
            />
          </View>
        }
        renderItem={({ item }) => (
          <ShowUsers
            name={item.name}
            img={item.profileImg}
            onImgTap={() => imgTap(item.profileImg, item.name)}
            onNameTap={() => nameTap(item.profileImg, item.name, item.id)}
          />
        )}
      />
      {/* <View style={{flexDirection: "column"}}>
          <View>{{name}}</View>
      </View> */}
    </SafeAreaView>
  );
};