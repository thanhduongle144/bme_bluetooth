import React, { useContext, useEffect, useState } from 'react'
import { View, Text, SafeAreaView, KeyboardAvoidingView, Keyboard, LogBox } from 'react-native'
import { globalStyle, color } from '../../utility';
import { keyboardVerticalOffset } from '../../utility/constants'
import { Logo, InputField, RoundCornerButton, decodeEmail } from '../../component'
import { Store } from '../../context/store';
import { LOADING_STOP, LOADING_START } from '../../context/actions/types';
import { SignUpRequest, AddUser } from '../../network';
import firebase from '../../firebase/config'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

const SignUp = ({ navigation }) => {
    const globalState = useContext(Store);
    const { mapLoaderState, dispatchLoaderAction } = globalState;

    const [credentials, setCredentials] = useState({
        name: '',
        email: '',
        password: '',
        confirmPassword: ''
    })
    const { name, email, password, confirmPassword } = credentials;
    const handleOnChange = (name, value) => {
        setCredentials({
            ...credentials,
            [name]: value,
        })
    }
    useEffect(() => {
        const highestTimeoutId = setTimeout(() => ';');
        for (let i = 0; i < highestTimeoutId; i++) {
            clearTimeout(i);
        }
    });
    const onSignUp = async () => {
        var re = /\S+@\S+\.\S+/;
        let data = await AsyncStorage.getItem('abc')
        console.warn(data)
        if (!re.test(email)) {
            alert('Where on Earth you use this email :)' + '\nPlease enter it correctly')
            return;
        } else if (!name) {
            alert('What\'s your name?')
            return
        } else if (password != confirmPassword) {
            alert('Confirm password is not correct!')
            return
        }
        if (!email) {
            alert('should input email');
        } else if (!password) {
            alert('should input password');
        } else {
            dispatchLoaderAction({
                type: LOADING_START
            })

            /*// THis use to auth using authenticate firebase method
            firebase.auth()
                .createUserWithEmailAndPassword(email, password)
                .then((signup) => {
                    if (signup && signup.additionalUserInfo.isNewUser) {
                        dispatchLoaderAction({
                            type: LOADING_STOP
                        })
                        alert('Sign up is completed.')
                    }
                })
            */

            let encode = decodeEmail(email);
            console.warn(encode)
            firebase.database().ref(encode)
                .once('value')
                .then(snapshot => {
                    // console.warn(snapshot.val())
                    if (snapshot.val() != null) {
                        alert('this email is used')
                        dispatchLoaderAction({
                            type: LOADING_STOP
                        })
                    } else {
                        let profileImg = ''
                        AddUser(name, email, password, profileImg)
                            .then((data) => {
                                dispatchLoaderAction({
                                    type: LOADING_STOP
                                })
                                alert('Sign up is completed.')
                                navigation.navigate('Login')
                            })
                            .catch(error => {
                                dispatchLoaderAction({
                                    type: LOADING_STOP
                                });
                                alert('Error:' + error)
                            })
                    }
                    // console.log('User data: ', snapshot.val().email);
                })
        }
    }
    LogBox.ignoreAllLogs = ['Setting a timer'];
    return (
        <SafeAreaView
            style={[globalStyle.flex1, { backgroundColor: color.DARK }]}
        >
            <View style={[globalStyle.containerCentered]}>
                <Logo />
            </View>
            <View style={[globalStyle.flex2, globalStyle.sectionCentered]}>
                <InputField
                    placeholder='Enter user name'
                    // keyboardType={'email-address'}
                    value={name}
                    onChangeText={(text) => handleOnChange('name', text)}
                />
                <InputField
                    placeholder='Enter email address'
                    keyboardType={'email-address'}
                    value={email}
                    onChangeText={(text) => handleOnChange('email', text)}
                />
                <InputField
                    placeholder='Enter password'
                    secureTextEntry={true}
                    value={password}
                    onChangeText={(text) => handleOnChange('password', text)}
                />
                <InputField
                    placeholder='Confirm password'
                    secureTextEntry={true}
                    value={confirmPassword}
                    onChangeText={(text) => handleOnChange('confirmPassword', text)}
                />
                <RoundCornerButton title='SignUp' onPress={() => onSignUp()} />
                <Text
                    style={{ color: 'orange', fontSize: 18 }}
                    onPress={() => { navigation.navigate('Login') }}
                >Login</Text>
            </View>
            {/* <Text onPress={()=>{navigation.navigate('SignUp')}}>Login</Text> */}
        </SafeAreaView>
    );
}

export default SignUp;