import React, { useContext, useState, useEffect } from 'react'
import { View, Text, SafeAreaView } from 'react-native'
import { globalStyle, color } from '../../utility';
import { Logo, InputField, RoundCornerButton, decodeEmail } from '../../component'
import { Store } from '../../context/store';
import { LOADING_STOP, LOADING_START } from '../../context/actions/types';
import firebase from '../../firebase/config'
import { setAsyncStorage, getAsyncStorage } from '../../asyncStorage';
import AsyncStorage from '@react-native-community/async-storage';

const Login = ({ navigation }) => {
    const globalState = useContext(Store);
    const { dispatchUserInfoAction, dispatchLoaderAction, mapLoaderState, mapUserInfoState } = globalState;

    const [credentials, setCredentials] = useState({
        email: '',
        password: '',
    })
    const { email, password } = credentials;
    const handleOnChange = (name, value) => {
        setCredentials({
            ...credentials,
            [name]: value,
        })
    }
    const onLogin = (emailStorage, passwordStorage) => {

        if (!email && !emailStorage) {
            alert('should input email');
        } else if (!password && !passwordStorage) {
            alert('should input password');
        } else {
            dispatchLoaderAction({
                type: LOADING_START
            })

            /*// THis use to auth using authenticate firebase method
            console.log('loadhere')
            firebase.auth()
                .signInWithEmailAndPassword(email ? email : emailStorage, password ? password : passwordStorage)
                .catch((error) => {
                    dispatchLoaderAction({
                        type: LOADING_STOP
                    })
                    console.log('error=' + error);
                    alert(error)
                })
            
                .then((mess) => {
                    dispatchLoaderAction({
                        type: LOADING_STOP
                    })
                    setAsyncStorage('user', JSON.stringify({
                        email: email ? email : emailStorage,
                        password: password,
                    }))
                    console.log(mess)
                })*/
            

            let encode = decodeEmail(email? email : emailStorage);
            console.log(encode)
            firebase.database().ref(encode)
                .once('value')
                .catch((error) => {
                    dispatchLoaderAction({
                        type: LOADING_STOP
                    })
                    console.log('error=' + error);
                    alert(error)
                })
                .then(async (snapshot) => {
                    console.warn(snapshot.val())
                    if (snapshot.val() != null) {
                        if (snapshot.val().save == password? password: passwordStorage) {
                            let profileImg = snapshot.val().profileImg
                            dispatchUserInfoAction({
                                type: "USER_DATA",
                                email: email? email: emailStorage,
                                name: snapshot.val().name,
                                encode: encode,
                                profileImg: profileImg
                            })
                            setAsyncStorage('user', JSON.stringify({
                                email: email? email: emailStorage,
                                password: snapshot.val().save,
                            }))
                            // dispatch(UserData('admin',encode,email,profileImg))
                            alert('Welcome ' + snapshot.val().name + '\nNice to see you again^^')
                            navigation.navigate('Dashboard')
                            dispatchLoaderAction({
                                type: LOADING_STOP
                            })
                        } else {
                            alert('Error password')
                            dispatchLoaderAction({
                                type: LOADING_STOP
                            })
                        }
                    } else {
                        alert('Haven\'t find this email TvT')
                        dispatchLoaderAction({
                            type: LOADING_STOP
                        })
                    }
                    // console.log('User data: ', snapshot.val().email);
                })

        }
    }
    useEffect(() => {
        // const highestTimeoutId = setTimeout(() => ';');
        // for (let i = 0; i < highestTimeoutId; i++) {
        //     clearTimeout(i);
        // }androi
        try {
            getAsyncStorage('user').then(async (data) => {
                if (data) {
                    console.log(JSON.parse(data).email)
                    await setCredentials({
                        ...credentials,
                        email: (JSON.parse(data).email),
                        password: (JSON.parse(data).password)
                    })
                    onLogin((JSON.parse(data).email), (JSON.parse(data).password));
                } else {
                    console.log(data)
                }
            })
            // console.warn('data='+JSON.stringify(data))
        } catch (err) {

        }
    }, [])
    return (
        <SafeAreaView
            style={[globalStyle.flex1, { backgroundColor: color.DARK }]}
        >
            <View style={[globalStyle.containerCentered]}>
                <Logo />
            </View>
            <View style={[globalStyle.flex2, globalStyle.sectionCentered]}>
                <InputField
                    placeholder='Enter email address'
                    keyboardType={'email-address'}
                    value={email}
                    onChangeText={(text) => handleOnChange('email', text)}
                />
                <InputField
                    placeholder='Enter password'
                    secureTextEntry={true}
                    value={password}
                    onChangeText={(text) => handleOnChange('password', text)}
                />
                <RoundCornerButton title='Login' onPress={() => onLogin()} />
                <Text
                    style={{ color: 'orange', fontSize: 18 }}
                    onPress={() => { navigation.navigate('SignUp') }}
                >Sign Up</Text>
            </View>
            {/* <Text onPress={()=>{navigation.navigate('SignUp')}}>Login</Text> */}
        </SafeAreaView>
    );
}

export default Login;