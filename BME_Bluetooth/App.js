  
import React from "react";
import Nav from "./src/navigation";
import Loader from "./src/component/loader";
import { StoreProvider } from "./src/context/store";
import { StatusBar } from "react-native";

console.disableYellowBox = true;

export default () => {
  return (
    <StoreProvider>
      <StatusBar barStyle="light-content" />
      <Nav />
      <Loader />
    </StoreProvider>
  );
};